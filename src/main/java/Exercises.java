import Model.Preposition;
import Model.SentenceOnePrep;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

public class Exercises {

    private SentenceDatabase sentenceDatabase;
    private ArrayList<Integer> numberArray;
    private static final int extraAnswers = 2;
    private ArrayList<SentenceOnePrep> filteredList;
    private Scanner scanner = new Scanner(System.in);

    public Exercises() {
        sentenceDatabase = new SentenceDatabase();
    }

    public static int getExtraAnswers() {
        return extraAnswers;
    }

    public void testOfSelectedPreps() {
        filteredList = selectedSentencesByPreposition(sentenceDatabase);
        System.out.println();
        System.out.println(filteredList);

    }

    private StringBuilder lineOfPreps() {
        StringBuilder lineOfPreps = new StringBuilder();
        String[] bank = sentenceDatabase.EnumIntoStringArray();
        for (int i = 0; i < bank.length; i++) {
            lineOfPreps.append(bank[i] + " ");
        }
        return lineOfPreps;
    }

    public ArrayList<SentenceOnePrep> selectedSentencesByPreposition(SentenceDatabase dataSet) {

        ArrayList<SentenceOnePrep> selectedItems = new ArrayList<>();
        int size = dataSet.getItems().size();
        System.out.println("Please choose a preposition from the following: " + lineOfPreps() + "." +
                "\nWhen you insert 'end' the selection will stop");
        String input;
        boolean run = true;
        String end = "end";
        while (run) {
            input = scanner.nextLine();
            if (input.equals(end))
                run = false;
            else
                for (int i = 0; i < size; i++) {
                    SentenceOnePrep sentence = dataSet.getItems().get(i);
                    if (input.equalsIgnoreCase(sentence.getAnswer().getLowCasePrep()) && !selectedItems.contains(sentence)) {
                        System.out.println(sentence.toString());
                        selectedItems.add(sentence);
                    }
                }

        }
        return selectedItems;
    }


    public void matchSentences(int numberOfSentences) {
        ArrayList<String> dividedSentence = new ArrayList<>();
        ArrayList<String> leftSet;
        ArrayList<String> rightSet;

        numberArray = generateDistinctiveNumbers(numberOfSentences, sentenceDatabase.getItems().size());
        for (int i = 0; i < numberArray.size(); i++) {
            dividedSentence.addAll(separateSentence(numberArray.get(i)));
        }
        leftSet = sortLeftOrRight(dividedSentence, false);
        rightSet = sortLeftOrRight(dividedSentence, true);
//        System.out.println(numberArray);
//        System.out.println(dividedSentence.toString());
//        System.out.println(leftSet);
//        System.out.println(rightSet);
        Collections.shuffle(rightSet);
        System.out.println("\tMatch " + numberOfSentences + " fragments on the left with those on the right.");
        for (int i = 0; i < numberOfSentences; i++) {
//            System.out.println((i + 1) + "." + leftSet.get(i) + "-\t\t\t\t\t -" + rightSet.get(i));
            System.out.printf("%-40s%5s\n", leftSet.get(i), rightSet.get(i));
        }
    }

    private ArrayList<String> sortLeftOrRight(ArrayList<String> sentences, boolean left) {
        ArrayList<String> sorted = new ArrayList<>();
        if (!left) {
            for (int i = 0; i < sentences.size(); i += 2) {
                sorted.add(sentences.get(i));
            }
        } else {
            int j = 0;
            for (int i = 1; i < sentences.size(); i += 2) {
                String noFloorSentence;
                noFloorSentence = sentences.get(i).replace("_____", sentenceDatabase.getItems()
                        .get(numberArray.get(j++)).getAnswer().getLowCasePrep());
                sorted.add(noFloorSentence);
            }
        }
        return sorted;
    }

    private ArrayList<String> separateSentence(int s) {
        ArrayList<String> toDivide = new ArrayList<>();
        String sentence = sentenceDatabase.getItems().get(s).getSentence();
        String[] tokens = sentence.split(" _");
        toDivide.addAll(Arrays.asList(tokens));
        return toDivide;
    }

    public void printAllAnswersOnTopPlusExtra(ArrayList<Integer> arrayList) {
        String[] hintArray = new String[arrayList.size() + extraAnswers];
        for (int i = 0; i < arrayList.size(); i++) {
            hintArray[i] = sentenceDatabase.getItems().get(arrayList.get(i)).getAnswer().getLowCasePrep();
        }
        for (int i = hintArray.length - extraAnswers; i < hintArray.length; i++) {
            hintArray[i] = sentenceDatabase.generateRandomPreposition(hintArray);
        }
        Collections.sort(Arrays.asList(hintArray));
//        sentenceDatabase.shuffleArray(hintArray);
        for (String i : hintArray) {
            System.out.print(i + " ");
        }
        System.out.println();
    }

    public void RandomXSentences(int numberOfSentences) {
        System.out.println("3 Random sentences\n");
        numberArray = generateDistinctiveNumbers(numberOfSentences, sentenceDatabase.getItems().size());
        printAllAnswersOnTopPlusExtra(numberArray);
        for (int i = 0; i < numberOfSentences; i++) {
            System.out.print(numberArray.get(i) + " ");
            sentenceDatabase.printGappedSentence(numberArray.get(i));
        }
//        for (Integer i : generateDistinctiveNumbers(numberOfSentences, )) {
//            randomNumbers[i] = i;
//            System.out.print(i+" ");
//        }
    }

    public ArrayList<Integer> generateDistinctiveNumbers(int numbersNeeded, int scope) {
        numberArray = new ArrayList<Integer>();
        for (int i = 0; i < numbersNeeded; i++) {
            boolean run = true;
            while (run) {
                int index = (int) (Math.random() * scope);
                if (!numberArray.contains(index)) {
                    numberArray.add(i, index);
                    run = false;
                }
            }
        }
        return numberArray;
    }
}
