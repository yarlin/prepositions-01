import java.util.Scanner;

public class Menu {

    private int choice;


    public void go() {
        System.out.print("Select option:" +
                "\n 1 - Generate test with all sentences" +
                "\n 2 - Generate test with 5 random sentences" +
                "\n 3 - Generate match sentences test" +
                "\n");
        Scanner scan = new Scanner(System.in);
        choice = scan.nextInt();
        switch (choice) {
            case 1:
                SentenceDatabase dupa = new SentenceDatabase();
                dupa.printAllSenAndCheckAnswer();
                break;
            case 2:
                Exercises randomNumberOfEx = new Exercises();
                randomNumberOfEx.RandomXSentences(5);
                break;
            case 3:
                Exercises matchSentences = new Exercises();
                matchSentences.matchSentences(3);
                break;
            default:
        }
    }
}
