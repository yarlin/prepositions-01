package Model;

public class SentenceOnePrep {
    private String sentence;
    private Preposition answer;
    private String floor = "______";

    public SentenceOnePrep(String sentence, Preposition answer) {
        this.sentence = sentence;
        this.answer = answer;
    }

    @Override
    public String toString() {
        String s1 = sentence.replace(floor, answer.getLowCasePrep());
        return s1;
    }

    public String getSentence() {
        return sentence;
    }

    public void setSentence(String sentence) {
        this.sentence = sentence;
    }

    public Preposition getAnswer() {
        return answer;
    }

    public void setAnswer(Preposition answer) {
        this.answer = answer;
    }

    public boolean isCorrect (String userInput, String answer){
        if (userInput.equalsIgnoreCase(answer))
            return true;
        return false;
    }
}
