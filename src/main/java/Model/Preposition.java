package Model;

public enum Preposition {
    ON("on"),
    IN("in"),
    AT("at"),
    OF("of"),
    WITH("with"),
    FROM("from"),
    FOR("for"),
    BY("by"),
    TO("to"),
    ABOUT("about"),
    OVER("over");

    private String lowCasePrep;

    public String getLowCasePrep() {
        return lowCasePrep;
    }

    Preposition(String lowCasePrep) {
        this.lowCasePrep = lowCasePrep;
    }

}