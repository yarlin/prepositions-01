import Model.Preposition;
import Model.SentenceOnePrep;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class SentenceDatabase {
    private static ArrayList<SentenceOnePrep> items;
    private String floor = "______";
    private final static int numberOfHints = 4;
    private String[] prepBank;
    private final static Preposition[] prepArray = Preposition.values();

    public SentenceDatabase() {
        items = new ArrayList<SentenceOnePrep>();
        items.add(new SentenceOnePrep("I think Martina is afraid ______ dogs.", Preposition.OF));
        items.add(new SentenceOnePrep("Can I pay ______ cheque, please?", Preposition.BY));
        items.add(new SentenceOnePrep("Dorota's living ______ a girl called Michelle.", Preposition.WITH));
        items.add(new SentenceOnePrep("Look at the exercise ______ the bottom of page 17.", Preposition.AT));
        items.add(new SentenceOnePrep("I usually have a little party ______ my birthday.", Preposition.ON));
        items.add(new SentenceOnePrep("What time did you arrive ______ London?", Preposition.AT));
//        Collections.shuffle(Arrays.asList(items));
    }

    public String[] getPrepBank() {
        return prepBank;
    }

    public void printAllSentencesWithGivenPreposition (String searchedPreposition){
        for (int i = 0 ; i<items.size(); i++){
            if (items.get(i).getSentence().contains(searchedPreposition))
                System.out.println(items.get(i) );
        }
    }

    public String generateRandomPreposition(String[] array){
        int index = (int) (Math.random() * (array.length - Exercises.getExtraAnswers()));
        String randomPrep = array[index];
                return randomPrep;
    }

    public ArrayList<SentenceOnePrep> getItems() {
        return items;
    }

    public String[] EnumIntoStringArray() {
        prepBank = new String[prepArray.length];
        for (int i = 0; i < prepArray.length; i++) {
            prepBank[i] = prepArray[i].getLowCasePrep();
        }
        return prepBank;
    }

    public void shuffleArray(String[] array) {
        for (int i = 0; i < array.length; i++) {
            int index = (int) (Math.random() * array.length);
            String temp = array[i];
            array[i] = array[index];
            array[index] = temp;
        }
    }

    public void printHints(Preposition answer) {
        String hintSet[] = new String[numberOfHints];
        shuffleArray(prepBank);
        for (int i = 0; i < hintSet.length; i++) {
            boolean run = true;
            while (run) {
                if (!prepBank[i].equals(answer.getLowCasePrep())) {
                    hintSet[i] = prepBank[i];
                } else hintSet[i] = prepBank[prepBank.length - i - 1];
                run = false;
            }
        }
        hintSet[hintSet.length - 1] = answer.getLowCasePrep();
        shuffleArray(hintSet);
        System.out.print("\t\t\t");
        for (int i = 0; i < hintSet.length; i++) {
            System.out.print(hintSet[i] + " ");
        }
    }

    public void printAllSenAndCheckAnswer() {
        Scanner input = new Scanner(System.in);
        System.out.println("Witaj, teraz poleci duzo zdan");
        shuffleArray(EnumIntoStringArray());
        Boolean[] isAnswerCorrect = new Boolean[items.size()];
        for (int i = 0; i < items.size(); i++) {
            System.out.print(i+1 + ". "+ items.get(i).getSentence() + "\n");
            printHints(items.get(i).getAnswer());
            System.out.println();
            String userAnswer = input.next();
            if (items.get(i).isCorrect(userAnswer, items.get(i).getAnswer().getLowCasePrep())) {
                System.out.println("That's right");
                isAnswerCorrect[i] = true;
            } else {
                System.out.println("Nahhh");
                isAnswerCorrect[i] = false;
            }
        }
        printReport(isAnswerCorrect);
    }

    public void printReport(Boolean[] answerArray) {
        int correctAnswer = 0;
        int incorrectAnswer = 0;
        for (int i = 0; i < answerArray.length; i++) {
            System.out.println((i + 1) + ". " + answerArray[i]);
            if (answerArray[i].equals(true)){
                correctAnswer++;
            }else
                incorrectAnswer++;
        }
        System.out.println(correctAnswer + "/" + answerArray.length );
    }

    public void printSentence(int i) {
        System.out.println(items.get(i));
    }

    public void printGappedSentence(int i){
        System.out.println(items.get(i).getSentence());
    }

    public void printAllSentences() {
        for (SentenceOnePrep sentence : items)
            System.out.println(sentence);
//                eg Dorota's living WITH a girl called Michelle.

    }

}
